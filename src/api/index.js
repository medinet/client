import{ getPatients, addPatient } from './patients'
import { getExtraRecordsByTempKey, addRecordByPubKey, getRecordsByPubKey, getFilesByRecordId, addFilesByRecordId, exportToIPFS } from './records'

export {
  getPatients,
  addPatient,
  getExtraRecordsByTempKey,
  addRecordByPubKey,
  getRecordsByPubKey,
  getFilesByRecordId,
  addFilesByRecordId,
  exportToIPFS
}