import axios from 'axios'
import { BASE_URL, PATIENTS } from '../constants/api'

export const getPatients = async () => {
  try {
    const res = await axios.get(`${BASE_URL}${PATIENTS}`)

    if (res.errorType || res.errorMessage) throw res

    return res.data
  } catch (err) {
    throw err.errorMessage || err.message
  }
}

export const addPatient = async patientData => {
  try {
    // const res = await axios.post(`${BASE_URL}${PATIENTS}`, patientData)
    // if (res.errorType || res.errorMessage) throw res
    //
    // return res.data

    return {
      "_id" : "5c03d795a9e3261204eee64335",
      "publicKey" : patientData.publicKey,
      "firstName" : "Test",
      "lastName" : "Test",
      "sex": "male",
      "phone": '(380)999-345335',
      "dob" : 1544187330723,
      "createdAt" : 1544187330723
    }
  } catch (err) {
    throw err.errorMessage || err.message
  }
}