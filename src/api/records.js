import axios from 'axios'

import { BASE_URL, RECORDS, PATIENTS } from '../constants/api'

export const getRecordsByPubKey = async pubKey => {
  try {
    const res = await axios.get(`${BASE_URL}${PATIENTS}/${pubKey}/medical-records`)
    if (res.errorType || res.errorMessage) throw res

    return res.data
  } catch (err) {
    throw err.errorMessage || err.message
  }
}

export const getFilesByRecordId = async (pubKey, recordId, files) => {
  try {
    const res = await axios.get(`${BASE_URL}${PATIENTS}/${pubKey}/medical-records/${recordId}/files`, files)
    if (res.errorType || res.errorMessage) throw res

    return res.data.map(path => `${BASE_URL}${path}`)
  } catch (err) {
    throw err.errorMessage || err.message
  }
}

export const addFilesByRecordId = async (pubKey, recordId, files) => {
  try {
    const res = await axios.post(`${BASE_URL}${PATIENTS}/${pubKey}/medical-records/${recordId}/files`, files)
    if (res.errorType || res.errorMessage) throw res

    return res.data.map(path => `${BASE_URL}${path}`)
  } catch (err) {
    throw err.errorMessage || err.message
  }
}

export const addRecordByPubKey = async (pubKey, record) => {
  try {
    const res = await axios.post(`${BASE_URL}${PATIENTS}/${pubKey}/medical-records`, record)
    if (res.errorType || res.errorMessage) throw res

    return res.data
  } catch (err) {
    throw err.errorMessage || err.message
  }
}

export const exportToIPFS = async (pubKey, recordId) => {
  try {
    const res = await axios.get(`${BASE_URL}${PATIENTS}/${pubKey}/medical-records/${recordId}/ipfs-export`)
    if (res.errorType || res.errorMessage) throw res

    return res.data.ipfsHash
  } catch (err) {
    throw err.errorMessage || err.message
  }
}

export const getExtraRecordsByTempKey = async tempKey => {
  try {
    const res = await axios.post(`${BASE_URL}${PATIENTS}`, {code: Number(tempKey)})
    if (res.errorType || res.errorMessage) throw res

    return res.data

    // return {
    //   publicKey: 'abc',
    //   records: [
    //     { "_id" : "5c04394992c6880a69d4e8as9", "publicKey" : "abc", "doctorName" : "Петренко Петро", "clinicName" : "Superclinic", "notes" : ['test', 'test'], "createdAt" : 1544187330723 },
    //     { "_id" : "5c04394992c6880a6as9d4e8ae", "publicKey" : "abc", "doctorName" : "Василенко Василь", "clinicName" : "Superclinic", "notes" : ['test', 'test'], "createdAt" : 1544187330723 }
    //   ]
    // }
  } catch (err) {
    throw err.errorMessage || err.message
  }
}
