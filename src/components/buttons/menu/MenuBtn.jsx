import React from 'react'
import { func, string } from 'prop-types'
import { IconButton } from '@material-ui/core'
import { Menu } from '@material-ui/icons'

const MenuBtn = ({ btnClass, menuToggle }) => (
  <IconButton
    color="inherit"
    aria-label="Open drawer"
    className={btnClass}
    onClick={menuToggle} >
    <Menu />
  </IconButton>
)

MenuBtn.propTypes = {
  btnClass: string,
  menuToggle: func
}

MenuBtn.defaultProps = {
  btnClass: '',
  menuToggle: null
}

export default MenuBtn
