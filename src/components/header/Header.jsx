import React from 'react'
import classNames from 'classnames'
import { object, string, number, node, bool } from 'prop-types'

import { withStyles } from '@material-ui/core/styles'
import { AppBar, Toolbar, Grid } from '@material-ui/core'

const styles = theme => ({
  root: {
    flexGrow: 1
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transitionProperty: 'all',
    transitionTimingFunction: theme.transitions.easing.easeIn,
    transitionDuration: theme.transitions.duration.short
  },
  primaryMain: {
    backgroundColor: theme.palette.primary.main
  },
  primaryDark: {
    backgroundColor: theme.palette.primary.dark
  },
  primaryLight: {
    backgroundColor: theme.palette.primary.light
  },
  secondaryMain: {
    backgroundColor: theme.palette.secondary.main
  },
  secondaryDark: {
    backgroundColor: theme.palette.secondary.dark
  },
  secondaryLight: {
    backgroundColor: theme.palette.secondary.light
  },
  transparent: {
    backgroundColor: 'transparent'
  }
})

class Header extends React.Component {
  static propTypes = {
    classes: object.isRequired,
    color: string,
    secondColor: string,
    changeColorOnHeight: number,
    disableGutters: bool,
    children: node
  }

  static defaultProps = {
    color: 'primaryMain',
    secondColor: 'primaryMain',
    changeColorOnHeight: null,
    disableGutters: false,
    children: null
  }

  constructor(props) {
    super(props)
    this.state = { useSecondaryColor: false }
  }

  componentDidMount() {
    if (this.props.changeColorOnHeight) {
      window.addEventListener('scroll', this.headerColorChange)
    }
  }

  componentWillUnmount() {
    if (this.props.changeColorOnHeight) {
      window.removeEventListener('scroll', this.headerColorChange)
    }
  }

  headerColorChange = () => {
    const { changeColorOnHeight } = this.props;
    const { useSecondaryColor } = this.state;
    const windowsScrollTop = window.pageYOffset;
    if (
      changeColorOnHeight
      && ((windowsScrollTop > changeColorOnHeight && !useSecondaryColor)
      || (windowsScrollTop < changeColorOnHeight && useSecondaryColor))
    ) this.setState(prevState => ({ useSecondaryColor: !prevState.useSecondaryColor }))
  }

  render() {
    const {
      classes,
      color,
      secondColor,
      disableGutters,
      children
    } = this.props;
    const { useSecondaryColor } = this.state;

    const appBarClasses = classNames({
      [classes.appBar]: true,
      [classes[color]]: !useSecondaryColor,
      [classes[secondColor]]: useSecondaryColor
    });

    return (
      <AppBar className={appBarClasses}>
        <Toolbar disableGutters={disableGutters} >
          <Grid
            alignItems="center"
            justify="space-between"
            container
            className={classes.root}>
            {children}
          </Grid>
        </Toolbar>
      </AppBar>
    )
  }
}

export default withStyles(styles)(Header);
