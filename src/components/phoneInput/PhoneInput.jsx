import React from 'react'
import MaskedInput from 'react-text-mask';


const PhoneInput = ({ inputRef, ...other }) => (
  <MaskedInput
    {...other}
    ref={inputRef}
    mask={['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]}
    placeholderChar={'\u2000'}
    showMask
  />
)

export default PhoneInput
