import React from 'react'
import { object } from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import {CircularProgress} from "@material-ui/core";

const styles = theme => ({
  progress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(50% 50%)'
  }
})

const Progress = ({ classes }) => (
  <CircularProgress className={classes.progress} />
)

Progress.propTypes = {
  classes: object.isRequired
}

export default withStyles(styles)(Progress)
