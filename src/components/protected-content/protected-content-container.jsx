import { connect } from 'react-redux'
import ProtectedContent from './protected-content'

const mapStateToProps = state => ({
  storeData: state.auth
})

export default connect(mapStateToProps)(ProtectedContent)
