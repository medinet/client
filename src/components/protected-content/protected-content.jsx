import React, { Fragment } from 'react'
import {
  array,
  element,
  func,
  oneOfType,
  string,
  node, object
} from 'prop-types'

import { arrayContainsArray } from '@root/utils'

const ProtectedContent = ({
  checkPermission,
  deniedContent,
  perm,
  storeData,
  children,
  identityPerm
}) => {
  if (checkPermission ? checkPermission(storeData) : arrayContainsArray(identityPerm, perm)) {
    return <Fragment>{children}</Fragment>
  }
  return deniedContent
}


ProtectedContent.propTypes = {
  checkPermission: func,
  storeData: object.isRequired,
  perm: array.isRequired,
  identityPerm: array.isRequired,
  children: oneOfType([element, string, func, array]).isRequired,
  deniedContent: node
}

ProtectedContent.defaultProps = {
  checkPermission: null,
  deniedContent: null
}

export default ProtectedContent
