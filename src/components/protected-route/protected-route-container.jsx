import { connect } from 'react-redux'
import ProtectedRoute from './protected-route'

const mapStateToProps = state => ({
  storeData: state.auth
})

export default connect(mapStateToProps)(ProtectedRoute)
