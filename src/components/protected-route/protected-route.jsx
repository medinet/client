import React from 'react'
import {
  array,
  element,
  func,
  oneOfType,
  string,
  object
} from 'prop-types'
import { Route, Redirect } from 'react-router-dom'

import { HOME } from '@root/constants/routes'
import { arrayContainsArray } from '@root/utils'

const ProtectedRoute = ({
  pathToRedirect,
  perm,
  storeData,
  component: Component,
  checkPermission,
  render,
  ...rest
}) => (
  <Route
    {...rest}
    render={props => {
      if (checkPermission ? checkPermission(storeData) : arrayContainsArray(storeData.data.perm, perm)) {
        return render ? render(props) : <Component {...props} />;
      }

      return (<Redirect to={pathToRedirect} />);
    }} />
)

ProtectedRoute.propTypes = {
  checkPermission: func,
  storeData: object.isRequired,
  perm: array,
  pathToRedirect: string,
  component: oneOfType([element, string, func, array]),
  render: func
}

ProtectedRoute.defaultProps = {
  checkPermission: null,
  perm: [],
  pathToRedirect: HOME,
  component: undefined,
  render: undefined
}

export default ProtectedRoute
