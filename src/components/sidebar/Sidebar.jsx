import React, { Fragment } from 'react'
import { object, func, bool, node } from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import classNames from 'classnames';
import { Drawer, Hidden } from '@material-ui/core'

const styles = theme => ({
  drawerPaper: {
    whiteSpace: 'nowrap',
    width: theme.spacing.unit * 30,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    width: theme.spacing.unit * 9
  }
})

const Sidebar = ({
  handleDrawerToggle,
  open,
  classes,
  children
}) => (
  <Fragment>
    <Hidden smDown>
      <Drawer
        variant="permanent"
        classes={{
          paper: classNames(classes.drawerPaper, !open && classes.drawerPaperClose)
        }}
        onClose={handleDrawerToggle}
        open={open} >
        {children}
      </Drawer>
    </Hidden>
    <Hidden mdUp>
      <Drawer
        variant="temporary"
        classes={{ paper: classes.drawerPaper }}
        open={open}
        onClose={handleDrawerToggle}
        ModalProps={{ keepMounted: true }} >
        {children}
      </Drawer>
    </Hidden>
  </Fragment>
)

Sidebar.propTypes = {
  classes: object.isRequired,
  children: node,
  handleDrawerToggle: func,
  open: bool
}

Sidebar.defaultProps = {
  handleDrawerToggle: null,
  children: null,
  open: false
}

export default withStyles(styles)(Sidebar);
