import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import Zoom from '@material-ui/core/Zoom';

const stylesSnackbar = theme => ({
  anchorOriginTopRight: {
    top: theme.spacing.unit * 8,
  }
});

const CustomSnackbar = withStyles(stylesSnackbar)(Snackbar);

const stylesSnackbarContent = theme => ({
  root: {
    border: '2px solid',
    backgroundColor: theme.palette.common.white
  },
  message: {
    padding: 0,
    fontSize: '1.1rem',
    fontFamily: theme.typography.fontFamily,
    fontWeight: 'bold',
  }
});

const CustomSnackbarContent = withStyles(stylesSnackbarContent)(SnackbarContent);

const styles = theme => ({
  success: {
    color: theme.palette.success.main,
    borderColor: theme.palette.success.main,
  },
  error: {
    color: theme.palette.error.light,
    borderColor: theme.palette.error.main,
  },
});

const CustomSnackbarComponent = ({ classes, open, type, message, onClose }) => (
  <CustomSnackbar
    anchorOrigin={{
      vertical: 'top',
      horizontal: 'right',
    }}
    TransitionComponent={Zoom}
    open={open}
    autoHideDuration={2000}
    onClose={onClose} >
    <CustomSnackbarContent
      className={`${classes.root} ${classes[type]}`}
      onClose={onClose}
      message={message} />
  </CustomSnackbar>
)

export default withStyles(styles)(CustomSnackbarComponent);
