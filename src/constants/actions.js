export const REQUEST = 'REQUEST'
export const SUCCESS = 'SUCCESS'
export const FAIL = 'FAIL'

export const OPEN_NOTIF = 'OPEN_NOTIF'
export const CLOSE_NOTIF = 'CLOSE_NOTIF'

export const LOGIN = 'LOGIN'
export const LOGOUT = 'LOGOUT'
export const CHECK_LOGIN = 'CHECK_LOGIN'

export const GET_RECORDS = 'GET_RECORDS'
export const GET_FILES = 'GET_FILES'
export const SET_NEW_RECORD = 'SET_NEW_RECORD'
export const ADD_RECORD = 'ADD_RECORD'
export const SELECT_RECORD = 'SELECT_RECORD'
export const ADD_EXTRA_RECORDS = 'ADD_EXTRA_RECORDS'

export const GET_PATIENTS = 'GET_PATIENTS'
export const ADD_PATIENT = 'ADD_PATIENT'
