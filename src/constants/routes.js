const HOME = '/'
const LOGIN = '/login'
const PATIENTS = '/patients'
const RECORDS = '/records'

export {
  HOME,
  LOGIN,
  PATIENTS,
  RECORDS
}
