import React, { Component } from 'react'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { func, object } from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import { Grid, Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, TextField, Typography, Paper, Divider } from '@material-ui/core'
import bip39 from 'bip39'

import Snackbar from '@root/components/snackbar'
import image from '@root/assets/img/logo.svg'
import { login, openNotif, closeNotif } from '@root/store/common/actions'

const styles = theme => ({
  root: {
    height: '100vh',
    width: '100vw',
    position: 'relative',
    padding: theme.spacing.unit * 4,
    '&::after': {
      content: '""',
      background: `url(${image}) center no-repeat`,
      backgroundSize: '120% 100%',
      opacity: 0.1,
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
      position: 'absolute',
      zIndex: -1,
    }
  },
  alert: {
    marginTop: theme.spacing.unit,
    color: theme.palette.error.main,
    fontSize: 10
  },
  dialogTitle: {
    fontSize: '20px',
    fontFamily: theme.typography.fontFamily,
    color: theme.palette.primary.dark
  },
  darkBody: {
    backgroundColor: theme.palette.primary.dark
  }
})

class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      modal: null,
      mnemonic: null
    }
  }

  openModal = type => this.setState({ modal: type })

  closeModal = () => this.setState({ modal: null, mnemonic: null })

  login = () => {
    const { login, openNotif } = this.props
    const { mnemonic } = this.state

    if(!bip39.validateMnemonic(mnemonic)) {
      openNotif({
        type: 'error',
        message: 'Invalid mnemonic seed'
      })
    } else {
      login(mnemonic)
    }

    this.closeModal()
  }

  register = () => this.setState({ modal: 'register', mnemonic: bip39.generateMnemonic() })

  handleEnterMnemonic = e => this.setState({ mnemonic: e.target.value })

  render() {
    const { classes, closeNotif, open, type, message } = this.props
    const { modal, mnemonic } = this.state

    return (
      <Grid
        container
        justify="center"
        className={classes.root}>
        <Grid container xs={12} sm={8} md={6} lg={4} justify="center" direction="column" spacing={32} item>
          <Grid item>
            <Button
              variant="contained"
              color="primary"
              fullWidth
              onClick={() => this.openModal('login')}>
              Login
            </Button>
          </Grid>
          <Grid item>
            <Button
              variant="contained"
              color="primary"
              fullWidth
              onClick={this.register}>
              Register
            </Button>
          </Grid>
        </Grid>
        <Dialog
          fullWidth
          open={modal === 'login'}
          onClose={this.closeModal} >
          <DialogTitle className={classes.dialogTitle} disableTypography>Enter mnemonic</DialogTitle>
          <DialogContent>
            <TextField
              autoFocus
              margin="dense"
              label="Mnemonic"
              type="text"
              onChange={this.handleEnterMnemonic}
              fullWidth />
          </DialogContent>
          <Divider className={classes.darkBody} variant="middle"/>
          <DialogActions>
            <Button onClick={this.closeModal} color="primary">
              Cancel
            </Button>
            <Button onClick={this.login} color="primary">
              Go to App
            </Button>
          </DialogActions>
        </Dialog>
        <Dialog
          fullWidth
          open={modal === 'register'}
          onClose={this.closeModal} >
          <DialogTitle className={classes.dialogTitle} disableTypography>Registration</DialogTitle>
          <DialogContent>
            <Paper>
              <DialogContentText>{mnemonic}</DialogContentText>
            </Paper>
            <Typography className={classes.alert} variant="h5" component="span">*Save your mnemonic seed!</Typography>
          </DialogContent>
          <Divider className={classes.darkBody} variant="middle"/>
          <DialogActions>
            <Button onClick={this.closeModal} color="primary">
              Cancel
            </Button>
            <Button onClick={this.login} color="primary">
              Go to App
            </Button>
          </DialogActions>
        </Dialog>
        <Snackbar open={open} type={type} message={message} onClose={closeNotif}/>
      </Grid>
    )
  }
}

Login.propTypes = {
  login: func.isRequired,
  openNotif: func.isRequired,
  classes: object.isRequired,
}

const mapStateToProps = state => ({
  open: state.notif.open,
  type: state.notif.type,
  message: state.notif.message
})

const mapDispatchToProps = {
  login,
  openNotif,
  closeNotif
}

export default compose(connect(mapStateToProps, mapDispatchToProps), withStyles(styles))(Login)
