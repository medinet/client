import React from 'react'
import { object } from 'prop-types'
import { compose } from 'redux'
import { withRouter } from "react-router-dom"
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import { Divider, List, ListItem, ListItemIcon, ListItemText, Grid } from '@material-ui/core'
import { Inbox, Drafts, Home, PersonAdd, Send, Settings, ExitToApp  } from '@material-ui/icons'

import { logout } from '@root/store/common/actions'
import { PATIENTS, RECORD } from '@root/constants/routes'

const styles = theme => ({
  wrap: { flex: 1 } ,
  selected: {
    color: theme.palette.primary.main
  }
})

const SidebarContent = ({ classes, logout, history, location, toggleDrawler }) => {
  const closeMenu = link => {
    // toggleDrawler()
    history.push(link)
  }

  return (
    <Grid container direction="column" justify="space-between" className={classes.wrap}>
      <Grid item direction="column" container>
        <List>
          <ListItem
            button
            selected={PATIENTS === location.pathname}
            onClick={() => closeMenu(PATIENTS)}>
            <ListItemIcon className={PATIENTS === location.pathname ? classes.selected : ''}>
              <Home />
            </ListItemIcon>
            <ListItemText className={PATIENTS === location.pathname ? classes.selected : ''} primary="History" />
          </ListItem>
          <ListItem
            button
            selected={`${PATIENTS}/new` === location.pathname}
            onClick={() => closeMenu(`${PATIENTS}/new`)}>
            <ListItemIcon className={`${PATIENTS}/new` === location.pathname ? classes.selected : ''}>
              <PersonAdd />
            </ListItemIcon>
            <ListItemText className={`${PATIENTS}/new` === location.pathname ? classes.selected : ''} primary="Scan" />
          </ListItem>
          <ListItem button>
            <ListItemIcon>
              <Send />
            </ListItemIcon>
            <ListItemText primary="Test" />
          </ListItem>
        </List>
        <Divider />
        <List>
          <ListItem button>
            <ListItemIcon>
              <Settings />
            </ListItemIcon>
            <ListItemText primary="Settings" />
          </ListItem>
          <ListItem button>
            <ListItemIcon>
              <Drafts />
            </ListItemIcon>
            <ListItemText primary="Help" />
          </ListItem>
        </List>
      </Grid>
      <Grid>
        <Divider/>
        <List>
          <ListItem button onClick={logout}>
            <ListItemIcon><ExitToApp /></ListItemIcon>
            <ListItemText primary="Logout" />
          </ListItem>
        </List>
      </Grid>
    </Grid>
  )
}

SidebarContent.propTypes = {
  classes: object.isRequired
}

const mapStateToProps = state => ({
  auth: state.auth
})

const mapDispatchToProps = {
  logout
}

export default compose(withRouter, withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(SidebarContent)
