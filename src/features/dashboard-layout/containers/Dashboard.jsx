import React, {Component} from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { object, arrayOf, node, oneOfType } from 'prop-types'
import { withStyles } from '@material-ui/core/styles';
import {Toolbar, Grid, Divider, Hidden} from '@material-ui/core';
import withWidth, { isWidthDown } from '@material-ui/core/withWidth';

import { closeNotif, getPatients } from '@root/store/common/actions'
import { MenuBtn } from '@root/components/buttons'
import Header from '@root/components/header'
import Sidebar from '@root/components/sidebar'
import Logo from '@root/components/logo'
import Snackbar from '@root/components/snackbar'

import SidebarContent from '../components/sidebar-content'


const styles = theme => ({
  root: {
    width: '100%',
    flexGrow: 1,
    height: '100vh',
    zIndex: 1,
    overflow: 'hidden',
    position: 'fixed',
    display: 'flex',
    flexDirection: 'column'
  },
  wrapper: {
    height: '100%',
    overflowX: 'hidden',
    overflowY: 'auto',
  },
  transition: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  widthOpened: {
    width: `calc(100% - ${theme.spacing.unit * 30}px)`
  },
  widthClosed: {
    width: `calc(100% - ${theme.spacing.unit * 9}px)`,
    [theme.breakpoints.down('sm')]: {
      width: '100%'
    }
  },
  menuButton: {
    marginLeft: theme.spacing.unit * 1,
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing.unit * 2
    }
  },
  hidden: {
    width: '100%',
    visibility: 'hidden'
  },
  nav: {
    zIndex: 1,
    position: 'fixed',
    bottom: 0,
    left: 0,
    right: 0
  },
  navItem: {
    width: '33.33%',
    maxWidth: 'initial',
    border: `1px solid ${theme.palette.primary.main}`,
    backgroundColor: theme.palette.white
  }
})

class MainLayout extends Component {
  static propTypes = {
    classes: object.isRequired,
    children: oneOfType([arrayOf(node), node]).isRequired
  }

  constructor(props) {
    super(props)
    this.state = { openDrawler: false }
  }

  componentDidMount() {
    this.props.getPatients()
  }

  toggleDrawler = () => this.setState(prevState => ({ openDrawler: !prevState.openDrawler }))

  render() {
    const { children, classes, closeNotif, open, type, message, width } = this.props
    const { openDrawler } = this.state

    return (
      <div className={classes.root}>
        <Header disableGutters={isWidthDown('sm', width)}>
          <Grid container alignItems="center" justify="flex-start">
            <Hidden mdUp>
              <MenuBtn btnClass={classes.menuButton} menuToggle={this.toggleDrawler} />
            </Hidden>
            <Logo color="white"/>
          </Grid>
        </Header>
        <Sidebar
          open={openDrawler}
          handleDrawerToggle={this.toggleDrawler} >
          <Hidden smDown><Toolbar /></Hidden>
          <Toolbar disableGutters >
            <Grid container alignItems="center">
              <Toolbar disableGutters >
                <Grid container alignItems="center">
                  <MenuBtn btnClass={classes.menuButton} menuToggle={this.toggleDrawler} />
                  <Hidden mdUp><Logo color="primary"/></Hidden>
                </Grid>
              </Toolbar>
            </Grid>
          </Toolbar>
          <Divider />
          <SidebarContent toggleDrawler={this.toggleDrawler}/>
        </Sidebar>
        <Toolbar className={classes.hidden}/>
        <Grid container justify="flex-end" className={classes.wrapper}>
          <Grid item className={`${classes.transition} ${classes[(openDrawler && width !== 'sm') ? 'widthOpened' : 'widthClosed']}`}>
            { children }
          </Grid>
        </Grid>
        <Snackbar open={open} type={type} message={message} onClose={closeNotif}/>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  open: state.notif.open,
  type: state.notif.type,
  message: state.notif.message
})

const mapDispatchToProps = {
  closeNotif,
  getPatients
}

export default compose(withWidth(), withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(MainLayout)
