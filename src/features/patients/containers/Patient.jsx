import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withRouter } from "react-router-dom"
import { withStyles } from '@material-ui/core/styles';
import {
  Paper,
  Table,
  TableHead,
  TablePagination,
  Typography,
  Grid,
  Button,
  TableRow,
  TableCell,
  TableBody,
  TableSortLabel, Dialog, DialogTitle, DialogContent, TextField, Divider, DialogActions, FormControl, InputLabel, Input
} from '@material-ui/core';
import moment from "moment";

import { getRecords, getExtraRecords } from '@root/store/common/actions'
import { RECORDS, PATIENTS } from '@root/constants/routes'
import Progress from '@root/components/progress'
import PhoneInput from '@root/components/phoneInput'
import { getSorting, stableSort } from '@root/utils'
import image from '@root/assets/img/logo.svg'

const styles = theme => ({
  maxWidth: {
    width: '100%'
  },
  title: {
    color: theme.palette.primary.dark,
    position: 'relative'
  },
  dialogTitle: {
    fontSize: '20px',
    fontFamily: theme.typography.fontFamily,
    color: theme.palette.primary.dark
  },
  wrapper: {
    padding: theme.spacing.unit * 2,
    position: 'relative'
  },
  toolbar: {
    display: 'flex',
    justifyContent: 'center'
  },
  darkBody: {
    backgroundColor: theme.palette.primary.dark
  },
  mr2: {
    marginRight: theme.spacing.unit
  },
  table: {
    position: 'relative',
    zIndex: 1,
    '&::after': {
      content: '""',
      background: `url(${image}) center no-repeat`,
      backgroundSize: 'auto 100%',
      opacity: 0.05,
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
      position: 'absolute',
      zIndex: -1,
    }
  },
  bold: {
    fontWeight: 'bold'
  },
  formControl: {
    margin: theme.spacing.unit,
  },
})

class Patient extends Component {
  constructor(props) {
    super(props)

    this.state = {
      selectedPatient: null,
      modalOpen: false,
      tempKey: null,
      order: 'asc',
      orderBy: 'createdAt',
      page: 0,
      rowsPerPage: 7,
    };
  }

  componentDidMount() {
    const { getRecords, match, patients } = this.props
    if(patients.fetching) return

    const pubKey = match.params['pubKey']
    this.setState({ selectedPatient: patients.all.find(patient => patient.publicKey === pubKey) })
    getRecords(pubKey)
  }

  componentDidUpdate(prevProps) {
    if(prevProps.patients !== this.props.patients) {
      const pubKey = this.props.match.params['pubKey']
      this.setState({ selectedPatient: this.props.patients.all.find(patient => patient.publicKey === pubKey) })
    }
  }

  handleRequestSort = property => {
    const orderBy = property;
    let order = 'desc';
    if (this.state.orderBy === property && this.state.order === 'desc') order = 'asc';
    this.setState({ order, orderBy });
  };

  handleChangePage = (event, page) => this.setState({ page })

  handleChangeRowsPerPage = event => this.setState({ rowsPerPage: event.target.value })

  setTempKey = event => this.setState({ tempKey: event.target.value })

  closeModal = () => this.setState({ modalOpen: false, tempKey: null })

  openModal = () => this.setState({ modalOpen: true })

  requestData = () => {
    const { selectedPatient, tempKey } = this.state
    if(!selectedPatient || !tempKey) return
    this.props.getExtraRecords(tempKey, selectedPatient.publicKey)
    this.closeModal()
  }

  render() {
    const { classes, history, records } = this.props
    const { order, orderBy, page, rowsPerPage, selectedPatient, modalOpen } = this.state

    const recordsNotEmpty = selectedPatient && records.byId[selectedPatient.publicKey]
    const rows = recordsNotEmpty ? records.byId[selectedPatient.publicKey].length : rowsPerPage
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows - page * rowsPerPage);

    return (
      <Grid container direction="column" alignItems="flex-start" className={classes.wrapper}>
        <Grid container style={{marginBottom: '20px'}} justify="space-between" alignItems="flex-end">
          <Typography className={classes.title} variant="h5" gutterBottom>
            {
              !selectedPatient
                ? <Progress/>
                : `${selectedPatient.firstName} ${selectedPatient.lastName}`
            }
          </Typography>
          <Grid>
            <Button
              variant="contained" color="primary"
              className={classes.mr2}
              onClick={this.openModal}>Import Medical Records</Button>
            <Button
              variant="contained" color="primary"
              onClick={() => selectedPatient && history.push(`${PATIENTS}/${selectedPatient.publicKey}${RECORDS}/new`)}>Add Record</Button>
          </Grid>
        </Grid>
        {
          records.fetching || !selectedPatient
          ? <Progress/>
          : <Grid container>
              <Grid container item xs={3} direction="column" alignItems="flex-start">
                <FormControl className={classes.formControl}>
                  <InputLabel htmlFor="phone-number">Phone number</InputLabel>
                  <Input
                    value={selectedPatient.phone}
                    disabled
                    id="phone-number"
                    inputComponent={PhoneInput}/>
                </FormControl>
                <FormControl className={classes.formControl}>
                  <InputLabel htmlFor="first-name">First Name</InputLabel>
                  <Input
                    value={selectedPatient.firstName}
                    disabled
                    id="first-name"/>
                </FormControl>
                <FormControl className={classes.formControl}>
                  <InputLabel htmlFor="last-name">Last Name</InputLabel>
                  <Input
                    value={selectedPatient.lastName}
                    disabled
                    id="last-name"/>
                </FormControl>
                <FormControl className={classes.formControl}>
                  <InputLabel htmlFor="dob">Birth Date</InputLabel>
                  <Input
                    value={selectedPatient.dob}
                    disabled
                    id="dob"/>
                </FormControl>
                <FormControl className={classes.formControl}>
                  <InputLabel htmlFor="sex">Sex</InputLabel>
                  <Input
                    value={selectedPatient.sex}
                    disabled
                    id="sex"/>
                </FormControl>
              </Grid>
              <Grid item xs={9}>
                <Paper square className={classes.maxWidth}>
                  <Table className={classes.table}>
                    <TableHead>
                      <TableRow>
                        <TableCell className={classes.bold} sortDirection={orderBy === 'clinicName' ? order : false}>
                          <TableSortLabel
                            active={orderBy === 'clinicName'}
                            direction={order}
                            onClick={() => this.handleRequestSort('clinicName')}>
                            Clinic Name
                          </TableSortLabel>
                        </TableCell>
                        <TableCell className={classes.bold} sortDirection={orderBy === 'doctorName' ? order : false}>
                          <TableSortLabel
                            active={orderBy === 'doctorName'}
                            direction={order}
                            onClick={() => this.handleRequestSort('doctorName')}>
                            Doctor Name
                          </TableSortLabel>
                        </TableCell>
                        <TableCell className={classes.bold}  padding="none">
                          Notes
                        </TableCell>
                        <TableCell className={classes.bold}>
                          <TableSortLabel
                            active={orderBy === 'createdAt'}
                            direction={order}
                            onClick={() => this.handleRequestSort('createdAt')}>
                            Date
                          </TableSortLabel>
                        </TableCell>
                        <TableCell numeric/>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {stableSort(recordsNotEmpty ? records.byId[selectedPatient.publicKey] : [], getSorting(order, orderBy))
                        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        .map(record => (
                          <TableRow key={record._id}>
                            <TableCell component="th" scope="row">{record.clinicName}</TableCell>
                            <TableCell>{record.doctorName}</TableCell>
                            <TableCell  padding="none">{record.notes[0]}</TableCell>
                            <TableCell>{moment(record.createdAt).format('DD.MM.YYYY')}</TableCell>
                            <TableCell numeric>
                              <Button
                                variant="contained" color="primary"
                                onClick={() => history.push(`${PATIENTS}/${selectedPatient.publicKey}${RECORDS}/${record._id}`)}>View</Button>
                            </TableCell>
                          </TableRow>
                        ))}
                      {emptyRows > 0 && (
                        <TableRow style={{height: 49 * emptyRows}}>
                          <TableCell colSpan={6}/>
                        </TableRow>
                      )}
                    </TableBody>
                  </Table>
                  <TablePagination
                    rowsPerPageOptions={[7, 14, 21]}
                    component="div"
                    count={recordsNotEmpty ? records.byId[selectedPatient.publicKey].length : 0}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onChangePage={this.handleChangePage}
                    onChangeRowsPerPage={this.handleChangeRowsPerPage}/>
                </Paper>
              </Grid>
            </Grid>
        }
        <Dialog
          fullWidth
          open={modalOpen}
          onClose={this.closeModal} >
          <DialogTitle className={classes.dialogTitle} disableTypography>Enter One Time Security Code</DialogTitle>
          <DialogContent>
            <TextField
              autoFocus
              margin="dense"
              label="Code"
              type="text"
              onChange={this.setTempKey}
              fullWidth />
          </DialogContent>
          <Divider className={classes.darkBody} variant="middle"/>
          <DialogActions>
            <Button onClick={this.closeModal} color="primary">
              Cancel
            </Button>
            <Button onClick={this.requestData} color="primary">
              Import
            </Button>
          </DialogActions>
        </Dialog>
      </Grid>
    )
  }
}

const mapStateToProps = state => ({
  patients: state.patients,
  records: state.records
})

const mapDispatchToProps = {
  getRecords,
  getExtraRecords
}

export default compose(withRouter, withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(Patient)
