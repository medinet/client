import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withRouter } from "react-router-dom"
import { withStyles } from '@material-ui/core/styles';
import {
  Typography,
  Grid, FormControl, Input, TextField, RadioGroup, FormControlLabel, FormLabel, Radio, Button
} from '@material-ui/core';

import { addPatient } from '@root/store/common/actions'
import { PATIENTS } from '@root/constants/routes'
import { getSorting, stableSort } from '@root/utils'
import PhoneInput from '@root/components/phoneInput'
import image from '@root/assets/img/logo.svg'

const styles = theme => ({
  title: {
    color: theme.palette.primary.dark,
    position: 'relative'
  },
  wrapper: {
    padding: theme.spacing.unit * 2,
    position: 'relative',
    zIndex: 1,
    '&::after': {
      content: '""',
      background: `url(${image}) center no-repeat`,
      backgroundSize: 'auto 100%',
      opacity: 0.05,
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
      position: 'absolute',
      zIndex: -1,
    }
  },
  formControl: {
    margin: theme.spacing.unit,
  },
  mr2: {
    marginRight: theme.spacing.unit
  },
  mt2: {
    marginTop: theme.spacing.unit
  }
})

class PatientNew extends Component {
  constructor(props) {
    super(props)

    this.state = {
      firstName: '',
      lastName: '',
      phone: `(   )    -      `,
      sex: 'male',
      dob: '',
      tempKey: ''
    };
  }

  handleChange = name => event => this.setState({ [name]: event.target.value })

  addPatient = () => {
    const { addPatient } = this.props
    const { tempKey, firstName, lastName, phone, sex, dob } = this.state

    if(!tempKey) return
    addPatient(tempKey, { firstName, lastName, phone, sex, dob })
  }

  render() {
    const { classes, history } = this.props
    const { phone, sex } = this.state
    return (
      <Grid container direction="column" alignItems="flex-start" className={classes.wrapper}>
        <Grid container style={{marginBottom: '20px'}} justify="space-between" alignItems="flex-end">
          <Typography className={classes.title} variant="h5" gutterBottom>New patient</Typography>
        </Grid>
        <Grid item container xs={12} justify="center">
          <Grid container  direction="column" item xs={8}>
            <FormControl className={classes.formControl}>
              <FormLabel component="legend">First Name</FormLabel>
              <Input
                onChange={this.handleChange('firstName')}
                id="first-name"/>
            </FormControl>
            <FormControl className={classes.formControl}>
              <FormLabel component="legend">Last Name</FormLabel>
              <Input
                onChange={this.handleChange('lastName')}
                id="last-name"/>
            </FormControl>
            <FormControl className={classes.formControl}>
              <FormLabel component="legend">Phone number</FormLabel>
              <Input
                id="phone-number"
                value={phone}
                onChange={this.handleChange('phone')}
                inputComponent={PhoneInput}/>
            </FormControl>
            <FormControl className={classes.formControl}>
              <FormLabel component="legend">Birthday</FormLabel>
              <TextField
                id="dob"
                type="date"
                onChange={this.handleChange('dob')}
                defaultValue="2018-12-07"
                InputLabelProps={{ shrink: true, }} />
            </FormControl>
            <FormControl className={classes.formControl}>
              <FormLabel component="legend">Sex</FormLabel>
              <Grid container>
                <RadioGroup
                  name="gender1"
                  className={classes.group}
                  value={sex}
                  onChange={this.handleChange('sex')} >
                  <FormControlLabel value="female" control={<Radio />} label="Female" />
                  <FormControlLabel value="male" control={<Radio />} label="Male" />
                </RadioGroup>
              </Grid>
            </FormControl>
            <FormControl className={classes.formControl}>
              <FormLabel component="legend">Temporary Key</FormLabel>
              <Input
                onChange={this.handleChange('tempKey')}
                id="tempKey"/>
            </FormControl>
          </Grid>
          <Grid item xs={12} container justify="center" spacing={24} className={classes.mt2}>
            <Grid item xs={8} container justify="flex-end">
              <Button
                variant="contained" color="primary"
                className={classes.mr2}
                onClick={() => history.push(`${PATIENTS}`)}>Cancel</Button>
              <Button
                variant="contained" color="primary"
                onClick={this.addPatient}>Add Patient</Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    )
  }
}

const mapDispatchToProps = {
  addPatient
}

export default compose(withRouter, withStyles(styles), connect(null, mapDispatchToProps))(PatientNew)
