import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withRouter } from "react-router-dom"
import { withStyles } from '@material-ui/core/styles';
import {
  Paper,
  Table,
  TableHead,
  TablePagination,
  Typography,
  Grid,
  Button,
  TableRow,
  TableCell,
  TableBody,
  TableSortLabel
} from '@material-ui/core';
import moment from "moment";

import { getRecordsByPubKey } from '@root/store/common/actions'
import Progress from '@root/components/progress'
import { getSorting, stableSort } from '@root/utils'
import { PATIENTS } from '@root/constants/routes'
import image from '@root/assets/img/logo.svg'

const styles = theme => ({
  maxWidth: {
    width: '100%'
  },
  title: {
    color: theme.palette.primary.dark,
    margin: 0,
    lineHeight: 0.8
  },
  dialogTitle: {
    fontSize: '20px',
    fontFamily: theme.typography.fontFamily,
    color: theme.palette.primary.dark
  },
  wrapper: {
    padding: theme.spacing.unit * 2,
    position: 'relative'
  },
  toolbar: {
    display: 'flex',
    justifyContent: 'center'
  },
  darkBody: {
    backgroundColor: theme.palette.primary.dark
  },
  table: {
    position: 'relative',
    zIndex: 1,
    '&::after': {
      content: '""',
      background: `url(${image}) center no-repeat`,
      backgroundSize: 'auto 100%',
      opacity: 0.05,
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
      position: 'absolute',
      zIndex: -1,
    }
  },
  bold: {
    fontWeight: 'bold'
  }
})

class Patients extends Component {
  constructor(props) {
    super(props)

    this.state = {
      order: 'asc',
      orderBy: 'createdAt',
      page: 0,
      rowsPerPage: 7,
    };
  }

  handleRequestSort = property => {
    const orderBy = property;
    let order = 'desc';
    if (this.state.orderBy === property && this.state.order === 'desc') order = 'asc';
    this.setState({ order, orderBy });
  };

  handleChangePage = (event, page) => this.setState({ page })

  handleChangeRowsPerPage = event => this.setState({ rowsPerPage: event.target.value })

  render() {
    const { classes, patients, history } = this.props
    const { order, orderBy, page, rowsPerPage } = this.state
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, patients.all.length - page * rowsPerPage);

    return (
      <Grid container direction="column" alignItems="flex-start" className={classes.wrapper}>
        <Grid container style={{marginBottom: '20px'}} justify="space-between" alignItems="flex-end">
          <Typography className={classes.title} variant="h5" gutterBottom>Patients</Typography>
          <Button
            variant="contained" color="primary"
            onClick={() => history.push(`${PATIENTS}/new`)}>Add patient</Button>
        </Grid>
        {
          patients.fetching
            ? <Progress/>
            : <Paper square className={classes.maxWidth}>
              <Table className={classes.table}>
                <TableHead>
                  <TableRow >
                    <TableCell className={classes.bold} sortDirection={orderBy === 'firstName' ? order : false}>
                      <TableSortLabel
                        active={orderBy === 'firstName'}
                        direction={order}
                        onClick={() => this.handleRequestSort('firstName')}>
                        First Name
                      </TableSortLabel>
                    </TableCell>
                    <TableCell className={classes.bold} sortDirection={orderBy === 'lastName' ? order : false}>
                      <TableSortLabel
                        active={orderBy === 'lastName'}
                        direction={order}
                        onClick={() => this.handleRequestSort('lastName')}>
                        Last Name
                      </TableSortLabel>
                    </TableCell>
                    <TableCell className={classes.bold} sortDirection={orderBy === 'dob' ? order : false}>
                      <TableSortLabel
                        active={orderBy === 'dob'}
                        direction={order}
                        onClick={() => this.handleRequestSort('dob')}>
                        Date of birthday
                      </TableSortLabel>
                    </TableCell>
                    <TableCell className={classes.bold} sortDirection={orderBy === 'sex' ? order : false}>
                      <TableSortLabel
                        active={orderBy === 'sex'}
                        direction={order}
                        onClick={() => this.handleRequestSort('sex')}>
                        Sex
                      </TableSortLabel>
                    </TableCell>
                    <TableCell className={classes.bold} sortDirection={orderBy === 'createdAt' ? order : false}>
                      <TableSortLabel
                        active={orderBy === 'createdAt'}
                        direction={order}
                        onClick={() => this.handleRequestSort('createdAt')}>
                        Registered
                      </TableSortLabel>
                    </TableCell>
                    <TableCell numeric/>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {stableSort(patients.all, getSorting(order, orderBy))
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map(patient => (
                      <TableRow key={patient.publicKey}>
                        <TableCell component="th" scope="row">{patient.firstName}</TableCell>
                        <TableCell>{patient.lastName}</TableCell>
                        <TableCell>{moment(patient.dob).format('DD.MM.YYYY')}</TableCell>
                        <TableCell>{patient.sex}</TableCell>
                        <TableCell>{moment(patient.createdAt).format('DD.MM.YYYY')}</TableCell>
                        <TableCell numeric>
                          <Button
                            variant="contained" color="primary"
                            onClick={() => history.push(`${PATIENTS}/${patient.publicKey}`)}>View</Button>
                        </TableCell>
                      </TableRow>
                    ))}
                  {emptyRows > 0 && (
                    <TableRow style={{ height: 49 * emptyRows }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>
              </Table>
              <TablePagination
                rowsPerPageOptions={[7, 14, 21]}
                component="div"
                count={patients.all.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onChangePage={this.handleChangePage}
                onChangeRowsPerPage={this.handleChangeRowsPerPage} />
            </Paper>
        }
      </Grid>
    )
  }
}

const mapStateToProps = state => ({
  patients: state.patients
})

const mapDispatchToProps = {
  getRecordsByPubKey
}

export default compose(withRouter, withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(Patients)
