import Patients from './containers/Patients'
import Patient from './containers/Patient'
import PatientNew from './containers/PatientNew'

export { Patients, Patient, PatientNew }
