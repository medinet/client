import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withRouter } from "react-router-dom"
import { withStyles } from '@material-ui/core/styles';
import {
  Typography,
  Grid, TextField, Button, CardMedia, Dialog, DialogTitle, DialogContent, Divider, DialogActions
} from '@material-ui/core';
import moment from "moment";
import QrCode from 'qrcode.react'

import { addRecord, getFiles, setNewRecordIPFS } from '@root/store/common/actions'
import { PATIENTS } from '@root/constants/routes'
import { getSorting, stableSort } from '@root/utils'

const styles = theme => ({
  title: {
    color: theme.palette.primary.dark,
    position: 'relative'
  },
  wrapper: {
    padding: theme.spacing.unit * 2,
  },
  formControl: {
    margin: theme.spacing.unit,
  },
  mr2: {
    marginRight: theme.spacing.unit
  },
  mt2: {
    marginTop: theme.spacing.unit
  },
  media: {
    height: theme.spacing.unit*20,
    margin: `0 ${theme.spacing.unit}px`,
    width: 'auto',
    backgroundPosition: "center center",
    backgroundSize: "auto 100%"
  },
})

class Record extends Component {
  constructor(props) {
    super(props)

    this.state = {
      selectedRecord: null,
      isNew: false,
      images: null,
      doctorName: '',
      note1: '',
      note2: '',
      note3: '',
      note4: ''
    };
  }

  componentDidMount() {
    const {records, match, history, getFiles} = this.props
    const { recordId, pubKey } = match.params

    if(recordId) {
      const selectedRecords = records.byId[pubKey]
      if(!selectedRecords) {
        if(!records.fetching) history.push(`${PATIENTS}`)
        return
      }
      const selectedRecord = selectedRecords.find(record => record._id === recordId)
      if(!selectedRecord) return

      getFiles(pubKey, recordId)

      this.setState({
        selectedRecord,
        images: selectedRecord.files,
        doctorName: selectedRecord.doctorName,
        note1: selectedRecord.notes[0],
        note2: selectedRecord.notes[1],
        note3: selectedRecord.notes[2],
        note4: selectedRecord.notes[3],
      })
    } else {
      this.setState({ isNew: true })
    }
  }

  componentDidUpdate(prevProps) {
    const {records, match} = this.props
    const { recordId, pubKey } = match.params

    if(records !== prevProps.records && !this.state.isNew) {
      const selectedRecords = records.byId[pubKey]
      if(!selectedRecords) return
      const selectedRecord = selectedRecords.find(record => record._id === recordId)
      if(!selectedRecord) return
      console.log(214124, selectedRecord);
      this.setState({
        selectedRecord,
        images: selectedRecord.files,
        note1: selectedRecord.notes[0],
        note2: selectedRecord.notes[1],
        note3: selectedRecord.notes[2],
        note4: selectedRecord.notes[3],
      })
    }
  }

  handleChange = name => event => this.setState({ [name]: event.target.value })

  addRecord = () => {
    const { match, addRecord } = this.props
    const { note1, note2, note3, note4, images, doctorName } = this.state
    const { pubKey } = match.params
    const notes = [note1, note2, note3, note4]

    addRecord(pubKey, {notes, images, doctorName})
  }

  addImages = event => this.setState({ images: event.target.files })

  closeModal = () => {
    const { setNewRecordIPFS } = this.props
    setNewRecordIPFS(null)
  }

  render() {
    const { classes, history, match, records } = this.props
    const { note1, note2, note3, note4, images, selectedRecord, isNew, doctorName } = this.state

    return (
      <Grid container direction="column" alignItems="flex-start" className={classes.wrapper}>
        <Grid container style={{marginBottom: '20px'}} justify="space-between" alignItems="flex-end">
          <Typography className={classes.title} variant="h5" gutterBottom>
            {isNew ? 'New record' : `${selectedRecord ? moment(selectedRecord.createdAt).format('DD.MM.YYYY') : ''}`}
          </Typography>
        </Grid>
        <Grid item container xs={12} justify="flex-start" spacing={24} className={classes.bgImage}>
          <Grid container item xs={6} direction="column">
            <TextField
              label="Diagnoses"
              multiline
              rows="10"
              disabled={!isNew}
              value={note1}
              onChange={this.handleChange('note1')}
              margin="normal"
              variant="outlined" />
            <TextField
              label="Analises"
              multiline
              rows="10"
              disabled={!isNew}
              value={note2}
              onChange={this.handleChange('note2')}
              margin="normal"
              variant="outlined" />
          </Grid>
          <Grid container item xs={6} direction="column">
            <TextField
              label="Farmacy"
              multiline
              rows="10"
              value={note3}
              disabled={!isNew}
              onChange={this.handleChange('note3')}
              margin="normal"
              variant="outlined" />
            <TextField
              label="Result"
              multiline
              rows="10"
              value={note4}
              disabled={!isNew}
              onChange={this.handleChange('note4')}
              margin="normal"
              variant="outlined" />
          </Grid>
          <Grid item xs={12} container className={classes.mt2}>
            <TextField
              label="Doctor Name"
              rows="1"
              value={doctorName}
              disabled={!isNew}
              onChange={this.handleChange('doctorName')}
              margin="normal"
              variant="outlined" />
          </Grid>
          <Grid item xs={12} container className={classes.mt2}>
            <Grid item xs={3}>
              <Button
                disabled={!isNew}
                variant="contained" color="primary"
                onClick={() => this.imagesRef.click()}>Attach files</Button>
            </Grid>
            <Grid item xs={9} container className={classes.mt2}>
              {
                images && (
                  isNew
                    ? Object.keys(images).map(key => (
                        <CardMedia
                          key={key}
                          className={classes.media}
                          image={URL.createObjectURL(images[key])}
                          component="img" />
                      ))
                    : images.map(path => (
                      <CardMedia
                        key={path}
                        className={classes.media}
                        image={path}
                        component="img" />
                    ))
                )
              }
            </Grid>
          </Grid>
          <Grid item xs={12} container justify="flex-end" spacing={24} className={classes.mt2}>
            <Button
              variant="contained" color="primary"
              className={classes.mr2}
              onClick={() => history.push(`${PATIENTS}/${match.params['pubKey']}`)}>Cancel</Button>
            {
              isNew && <Button
                variant="contained" color="primary"
                onClick={this.addRecord}>Add Record</Button>
            }
          </Grid>
        </Grid>
        <input
          accept="image/*"
          style={{ display: 'none' }}
          ref={el => {this.imagesRef = el}}
          multiple
          onChange={this.addImages}
          type="file" />
        <Dialog
          fullWidth
          open={!!records.newRecordIPFS}
          onClose={this.closeModal} >
          <DialogTitle className={classes.dialogTitle} disableTypography>QR code</DialogTitle>
          <DialogContent>
            {
              records.newRecordIPFS && <QrCode size={200} value={records.newRecordIPFS} />
            }
          </DialogContent>
          <Divider className={classes.darkBody} variant="middle"/>
          <DialogActions>
            <Button onClick={this.closeModal} color="primary">
              ok
            </Button>
          </DialogActions>
        </Dialog>
      </Grid>
    )
  }
}

const mapStateToProps = state => ({
  records: state.records
})

const mapDispatchToProps = {
  addRecord,
  getFiles,
  setNewRecordIPFS
}

export default compose(withRouter, withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(Record)
