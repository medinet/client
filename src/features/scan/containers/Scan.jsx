import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles';
import QrReader from "react-qr-reader";
import {Typography, Grid, Button, Card, CardActions, CardContent } from '@material-ui/core';

import { saveRecord } from '@root/store/common/actions'
import moment from "moment"

const styles = theme => ({
  padding: {
    height: '100%',
    padding: theme.spacing.unit * 2,
    overflowX: 'hidden',
    overflowY: 'auto',
  },
  dialogTitle: {
    width: '100%',
    color: theme.palette.primary.dark
  },
  qr: {
    width: '100%',
    maxWidth: 400
  },
  bold: {
    fontWeight: 'bold'
  }
})



class Scan extends Component {
  constructor() {
    super()

    this.state = {
      selectedData: {
        link: 'asdgsd',
        data: {
          _id: 'safd',
          createdAt: 1543778141574,
          doctorName: 'Petrov Ivan Nikolaevich',
          clinicName: 'Medical Center',
          notes: 'Lorem Ipsum is simply dummy text of the printing and typley of type and scramblspecimen book. ILorem Ipsum is simply dummy text of the printing and typley of type and scramblspecimen book. ILorem Ipsum is simply dummy text of the printing and typley of type and scramblspecimen book. ILorem Ipsum is simply dummy text of the printing and typley of type and scramblspecimen book. ILorem Ipsum is simply dummy text of the printing and typley of type and scramblspecimen book. ILorem Ipsum is simply dummy text of the printing and typley of type and scramblspecimen book. ILorem Ipsum is simply dummy text of the printing and typley of type and scramblspecimen book. ILorem Ipsum is simply dummy text of the printing and typley of type and scramblspecimen book. ILorem Ipsum is simply dummy text of the printing and typley of type and scramblspecimen book. ILorem Ipsum is simply dummy text of the printing and typley of type and scramblspecimen book. ILorem Ipsum is simply dummy text of the printing and typley of type and scramblspecimen book. ILorem Ipsum is simply dummy text of the printing and typley of type and scramblspecimen book. ILorem Ipsum is simply dummy text of the printing and typley of type and scramblspecimen book. ILorem Ipsum is simply dummy text of the printing and typley of type and scramblspecimen book. ILorem Ipsum is simply dummy text of the printing and typley of type and scramblspecimen book. ILorem Ipsum is simply dummy text of the printing and typley of type and scramblspecimen book. ILorem Ipsum is simply dummy text of the printing and typley of type and scramblspecimen book. ILorem Ipsum is simply dummy text of the printing and typley of type and scramblspecimen book. ILorem Ipsum is simply dummy text of the printing and typley of type and scramblspecimen book. I',
          publicKey: 'ssda34c35v'
        }
      }
    };
  }

  handleScan = data => {
    if (data) this.setState({selectedData: data})
  }


  render() {
    const { classes, saveRecord } = this.props
    const { selectedData } = this.state

    return (
      <Grid container className={classes.padding}>
        <Typography className={classes.dialogTitle} variant="h5" gutterBottom>Scan QR</Typography>
        {
          !selectedData
            ? <QrReader
              onError={() => null}
              onScan={this.handleScan}
              className={classes.qr} />
            : <Card>
              <CardContent>
                <Typography className={classes.bold} variant="h5" gutterBottom>{selectedData.data.clinicName}</Typography>
                <Typography variant="h6" gutterBottom>
                  Doctor: <span className={classes.bold}>{selectedData.data.doctorName}</span>
                </Typography>
                <Typography variant="subtitle1" gutterBottom>{selectedData.data.notes}</Typography>
                <Typography variant="subtitle2" >Date: {moment(selectedData.data.createdAt).format('DD MMM YYYY')}</Typography>
              </CardContent>
              <CardActions>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={() => saveRecord(selectedData)}>
                  Save record
                </Button>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={() => this.setState({selectedData: null})}>
                  Cancel
                </Button>
              </CardActions>
            </Card>
        }
      </Grid>
    )
  }
}

const mapDispatchToProps = {
  saveRecord
}

export default compose(withStyles(styles), connect(null, mapDispatchToProps))(Scan)
