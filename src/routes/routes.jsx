import React from 'react'
import { Switch, Redirect, Route } from 'react-router'

import ProtectedRoute from '@root/components/protected-route'
import { Dashboard } from '@root/features/dashboard-layout'

import { LOGIN, HOME, PATIENTS, RECORDS } from '@root/constants/routes'
import { Login } from '@root/features/auth'
import { Patients, Patient, PatientNew } from '@root/features/patients'
import { Record } from '@root/features/record'

const MainRoutes = () => (
  <Dashboard>
    <Switch>
      <Route exact path={PATIENTS} component={Patients} />
      <Route exact path={`${PATIENTS}/new`} component={PatientNew} />
      <Route exact path={`${PATIENTS}/:pubKey`} component={Patient} />
      <Route exact path={`${PATIENTS}/:pubKey${RECORDS}/new`} component={Record} />
      <Route exact path={`${PATIENTS}/:pubKey${RECORDS}/:recordId`} component={Record} />
      <Redirect from="/" to={PATIENTS} />
    </Switch>
  </Dashboard>
)

const Routes = () => (
  <Switch>
    <ProtectedRoute
      exact
      path={LOGIN}
      pathToRedirect={HOME}
      checkPermission={store => !store.isLoggedIn}
      component={Login} />
    <ProtectedRoute
      path={HOME}
      pathToRedirect={LOGIN}
      checkPermission={store => store.isLoggedIn}
      component={MainRoutes} />
  </Switch>
)

export default Routes
