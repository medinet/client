import { LOGIN, LOGOUT, REQUEST, CHECK_LOGIN } from '@root/constants/actions'

export const checkLogin = () => ({
  type: `${CHECK_LOGIN}`
})

export const login = mnemonic => ({
  type: `${REQUEST}_${LOGIN}`,
  mnemonic
})

export const logout = () => ({
  type: `${REQUEST}_${LOGOUT}`
})
