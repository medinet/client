import { login, logout, checkLogin } from './auth'
import { openNotif, closeNotif } from './notif'
import { getExtraRecords, addRecord, getRecords, getFiles, setNewRecordIPFS } from './records'
import { getPatients, addPatient } from './patients'

export {
  login,
  logout,
  checkLogin,
  openNotif,
  closeNotif,
  getExtraRecords,
  addRecord,
  getRecords,
  getPatients,
  addPatient,
  getFiles,
  setNewRecordIPFS
}
