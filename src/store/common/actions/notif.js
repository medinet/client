import { OPEN_NOTIF, CLOSE_NOTIF } from '@root/constants/actions'

export const openNotif = notif => ({
  type: `${OPEN_NOTIF}`,
  notif
})

export const closeNotif = () => ({
  type: `${CLOSE_NOTIF}`
})
