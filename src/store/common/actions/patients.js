import { GET_PATIENTS, ADD_PATIENT, SELECT_PATIENT, REQUEST } from '@root/constants/actions'

export const getPatients = () => ({
  type: `${REQUEST}_${GET_PATIENTS}`
})

export const addPatient = (tempKey, patientData) => ({
  type: `${REQUEST}_${ADD_PATIENT}`,
  tempKey,
  patientData
})