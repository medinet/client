import { GET_RECORDS, ADD_EXTRA_RECORDS, ADD_RECORD, REQUEST, GET_FILES, SET_NEW_RECORD } from '@root/constants/actions'

export const getRecords = publicKey => ({
  type: `${REQUEST}_${GET_RECORDS}`,
  publicKey
})

export const getExtraRecords = (tempKey, publicKey) => ({
  type: `${REQUEST}_${ADD_EXTRA_RECORDS}`,
  publicKey,
  tempKey
})

export const addRecord = (publicKey, recordData) => ({
  type: `${REQUEST}_${ADD_RECORD}`,
  publicKey,
  recordData
})

export const getFiles = (publicKey, recordId) => ({
  type: `${REQUEST}_${GET_FILES}`,
  publicKey,
  recordId
})

export const setNewRecordIPFS = (newRecordIPFS) => ({
  type: `${SET_NEW_RECORD}`,
  newRecordIPFS
})