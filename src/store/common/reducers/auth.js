import { LOGIN, LOGOUT } from '@root/constants/actions'

const initialState = {
  isLoggedIn: false,
  mnemonic: null
}

export default (state = initialState, action) => {
  switch (action.type) {
    case `${LOGIN}`:
      return {
        isLoggedIn: true,
        mnemonic: action.payload.mnemonic
      }
    case `${LOGOUT}`:
      return { ...initialState }
    default:
      return state
  }
}
