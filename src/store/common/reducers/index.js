import auth from './auth'
import notif from './notif'
import records from './records'
import patients from './patients'

export default {
  auth,
  notif,
  records,
  patients
}
