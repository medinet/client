import { OPEN_NOTIF, CLOSE_NOTIF } from '@root/constants/actions'

const initialState = {
  open: false,
  type: 'success',
  message: ''
}

export default (state = initialState, action) => {
  switch (action.type) {
    case `${OPEN_NOTIF}`:
      return {
        open: true,
        type: action.notif.type,
        message: action.notif.message,
      }
    case `${CLOSE_NOTIF}`:
      return { ...state, open: false }
    default:
      return state
  }
}
