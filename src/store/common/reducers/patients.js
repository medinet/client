import { SUCCESS, FAIL, REQUEST, LOGOUT, GET_PATIENTS, ADD_PATIENT } from '@root/constants/actions'
import { mapBy } from '@root/utils'

const initialState = {
  all: [],
  byId: {},
  selected: null,
  fetching: false,
  error: null
}

export default (state = initialState, action) => {
  switch (action.type) {
    case `${REQUEST}_${GET_PATIENTS}`:
      return { ...initialState, fetching: true }
    case `${SUCCESS}_${GET_PATIENTS}`:
      return { ...state, fetching: false, all: action.patients, byId: mapBy(action.patients, 'publicKey') }
    case `${FAIL}_${GET_PATIENTS}`:
      return { ...state, fetching: false, error: action.error }

    case `${REQUEST}_${ADD_PATIENT}`:
      return { ...initialState, fetching: true }
    case `${SUCCESS}_${ADD_PATIENT}`:
      return { ...state, fetching: false, all: [...state.all, action.patient], byId: {...state.byId, [action.patient.publicKey]: action.patient} }
    case `${FAIL}_${ADD_PATIENT}`:
      return { ...state, fetching: false, error: action.error }

    case `${LOGOUT}`:
      return { ...initialState }
    default:
      return state
  }
}
