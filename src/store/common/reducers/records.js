import { SUCCESS, FAIL, REQUEST, LOGOUT, GET_RECORDS, ADD_RECORD, ADD_EXTRA_RECORDS, SELECT_RECORD, GET_FILES, SET_NEW_RECORD } from '@root/constants/actions'

const initialState = {
  byId: {},
  fetching: false,
  newRecordIPFS: null,
  error: null
}

export default (state = initialState, action) => {
  switch (action.type) {
    case `${REQUEST}_${GET_RECORDS}`:
      return { ...state, fetching: true }
    case `${SUCCESS}_${GET_RECORDS}`:
      return { ...state, fetching: false, byId: {...state.byId, [action.publicKey]: action.records} }
    case `${FAIL}_${GET_RECORDS}`:
      return { ...state, fetching: false, error: action.error }

    case `${REQUEST}_${ADD_RECORD}`:
      return { ...state, fetching: true, error: null }
    case `${SUCCESS}_${ADD_RECORD}`:
      return { ...state, fetching: false, byId: {...state.byId, [action.publicKey]: [...(state.byId[action.publicKey] || []), action.record]} }
    case `${FAIL}_${ADD_RECORD}`:
      return { ...state, fetching: false, error: action.error }

    case `${REQUEST}_${ADD_EXTRA_RECORDS}`:
      return { ...state, fetching: true, error: null }
    case `${SUCCESS}_${ADD_EXTRA_RECORDS}`:
      return { ...state, fetching: false, byId: {...state.byId, [action.publicKey]: [...(state.byId[action.publicKey] || []), ...action.records]} }
    case `${FAIL}_${ADD_EXTRA_RECORDS}`:
      return { ...state, fetching: false, error: action.error }

    case `${REQUEST}_${GET_FILES}`:
      return { ...state, fetching: true, error: null }
    case `${SUCCESS}_${GET_FILES}`:
      return {
        ...state,
        fetching: false,
        byId: {
          ...state.byId,
          [action.publicKey]: [...(state.byId[action.publicKey] || []).map(
            record => record._id === action.recordId ? { ...record, files: action.files} : record
            )]
        }
      }
    case `${FAIL}_${GET_FILES}`:
      return { ...state, fetching: false, error: action.error }

    case `${SET_NEW_RECORD}`:
      return { ...initialState, newRecordIPFS: action.newRecordIPFS }

    case `${LOGOUT}`:
      return { ...initialState }
    default:
      return state
  }
}
