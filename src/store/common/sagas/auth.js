import { put, takeLatest } from 'redux-saga/effects'

import { LOGIN, LOGOUT, CHECK_LOGIN, REQUEST, UPDATE_RECORDS } from '@root/constants/actions'
import { HOME } from '@root/constants/routes'
import { StoredMnemonic, StoredRecords, DidManager } from '@root/utils'
import { store } from '@root/store'
import { getIndex, getRecordsFromBlockchain } from '@root/api'


export function* login({ mnemonic }) {
  StoredMnemonic.set(mnemonic)

  yield put({ type: `${LOGIN}`, payload: { mnemonic } })
}

export function* logout() {
  StoredMnemonic.remove()

  yield put({ type: `${LOGOUT}` })
}

export function* checkLogin() {
  const mnemonic = StoredMnemonic.get()

  if(!mnemonic) return
  yield put({ type: `${REQUEST}_${LOGIN}`, mnemonic })
}



export default function accountsSaga() {
  return [
    takeLatest(`${REQUEST}_${LOGIN}`, login),
    takeLatest(`${REQUEST}_${LOGOUT}`, logout),
    takeLatest(`${CHECK_LOGIN}`, checkLogin)
  ]
}
