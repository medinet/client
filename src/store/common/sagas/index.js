import auth from './auth'
import records from './records'
import patients from './patients'

export default function () {
  return [
    ...auth(),
    ...records(),
    ...patients()
  ]
}
