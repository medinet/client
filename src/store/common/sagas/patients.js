import { put, takeLatest, call } from 'redux-saga/effects'
import { push } from 'react-router-redux';

import { GET_PATIENTS, ADD_PATIENT, REQUEST, SUCCESS, FAIL, ADD_EXTRA_RECORDS } from '@root/constants/actions'
import { getPatients as getPatientsApi, getExtraRecordsByTempKey, addPatient as addPatientApi } from '@root/api'
import { PATIENTS } from '@root/constants/routes'

export function* getPatients() {
  try {
    const patients = yield call(getPatientsApi)

    yield put({ type: `${SUCCESS}_${GET_PATIENTS}`, patients })
  } catch (err) {
    yield put({ type: `${FAIL}_${GET_PATIENTS}`, error: err.message || err })
  }
}

export function* addPatient({ tempKey, patientData }) {
  try {
    const { records, publicKey } = yield call(getExtraRecordsByTempKey, tempKey)
    yield put({ type: `${SUCCESS}_${ADD_EXTRA_RECORDS}`, records, publicKey })

    const patient = yield call(addPatientApi, { ...patientData, publicKey })

    yield put({ type: `${SUCCESS}_${ADD_PATIENT}`, patient })
    yield put(push(`${PATIENTS}/${patient.publicKey}`));
  } catch (err) {
    yield put({ type: `${FAIL}_${ADD_PATIENT}`, error: err.message || err })
  }
}

export default function patientsSaga() {
  return [
    takeLatest(`${REQUEST}_${GET_PATIENTS}`, getPatients),
    takeLatest(`${REQUEST}_${ADD_PATIENT}`, addPatient)
  ]
}
