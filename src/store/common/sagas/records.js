import { put, takeLatest, select, call } from 'redux-saga/effects'

import { GET_RECORDS, REQUEST, SUCCESS, FAIL, ADD_EXTRA_RECORDS, GET_FILES, ADD_RECORD, SET_NEW_RECORD } from '@root/constants/actions'
import { getRecordsByPubKey, getExtraRecordsByTempKey, getFilesByRecordId, addRecordByPubKey, addFilesByRecordId, exportToIPFS } from '@root/api'

export function* getRecords({ publicKey }) {
  try{
    const records = yield call(getRecordsByPubKey, publicKey)

    yield put({ type: `${SUCCESS}_${GET_RECORDS}`, records, publicKey })
  } catch (error) {
    yield put({ type: `${FAIL}_${GET_RECORDS}`, error })
  }
}

export function* getFiles({ publicKey, recordId }) {
  try{
    const files = yield call(getFilesByRecordId, publicKey, recordId)

    yield put({ type: `${SUCCESS}_${GET_FILES}`, files, publicKey, recordId })
  } catch (error) {
    yield put({ type: `${FAIL}_${GET_FILES}`, error })
  }
}

export function* addRecord({ publicKey, recordData }) {
  try{
    const record = yield call(addRecordByPubKey, publicKey, {doctorName: recordData.doctorName, notes: recordData.notes})
    yield put({ type: `${SUCCESS}_${ADD_RECORD}`, publicKey, record })

    const formData = new FormData()
    for(let i = 0; i < recordData.images.length; i++) {
      formData.append('files', recordData.images[i])
    }

    const files = yield call(addFilesByRecordId, publicKey, record._id, formData)
    yield put({ type: `${SUCCESS}_${GET_FILES}`, files, publicKey, recordId: record._id })
    const newRecordIPFS = yield call(exportToIPFS, publicKey, record._id)
    yield put({ type: `${SET_NEW_RECORD}`, newRecordIPFS })
  } catch (error) {
    yield put({ type: `${FAIL}_${ADD_RECORD}`, error })
  }
}

export function* requestExtraRecords({ publicKey, tempKey }) {
  try{
    yield call(getExtraRecordsByTempKey, tempKey)
    yield put({ type: `${REQUEST}_${GET_RECORDS}`, publicKey })
  } catch (error) {
    yield put({ type: `${FAIL}_${ADD_EXTRA_RECORDS}`, error })
  }
}

export default function recordsSaga() {
  return [
    takeLatest(`${REQUEST}_${GET_RECORDS}`, getRecords),
    takeLatest(`${REQUEST}_${ADD_EXTRA_RECORDS}`, requestExtraRecords),
    takeLatest(`${REQUEST}_${GET_FILES}`, getFiles),
    takeLatest(`${REQUEST}_${ADD_RECORD}`, addRecord)
  ]
}
