import { applyMiddleware, compose, createStore } from 'redux'
import { routerMiddleware } from 'react-router-redux'
import createHistory from 'history/createBrowserHistory'
import createSagaMiddleware from 'redux-saga'
import commonReducers from './reducers'
import rootSaga from './sagas'
import { checkLogin } from './common/actions'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
export const history = createHistory()
const sagaMiddleware = createSagaMiddleware()
export const store = createStore(
  commonReducers,
  composeEnhancers(applyMiddleware(
    sagaMiddleware,
    routerMiddleware(history)
  ))
)

sagaMiddleware.run(rootSaga)
store.dispatch(checkLogin())
