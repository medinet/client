import { combineReducers } from 'redux'
import { routerReducer as router } from 'react-router-redux'

import { reducers as common } from './common'

const commonReducers = combineReducers({
  ...common,
  router
})

export default commonReducers
