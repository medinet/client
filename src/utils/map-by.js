export const mapBy = (arr, key) => arr.reduce((res, item) => ({ ...res, [item[key]]: item }), {})
