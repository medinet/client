export class LocalStorageItem {
  constructor(key) {
    this.storageKey = key
    this.localStorageSupported = typeof window.localStorage !== 'undefined' && window.localStorage !== null
  }

  set(item) {
    if (this.localStorageSupported) localStorage.setItem(this.storageKey, JSON.stringify(item))
  }

  get() {
    return this.localStorageSupported ? JSON.parse(localStorage.getItem(this.storageKey)) : null
  }

  remove() {
    if (this.localStorageSupported) localStorage.removeItem(this.storageKey)
  }
}

export const StoredMnemonic = new LocalStorageItem('mnemonic')
