import { createMuiTheme } from '@material-ui/core/styles'

export const theme = createMuiTheme({
  typography: {
    useNextVariants: true,
  },
  palette: {
    primary: {
      light: '#24982f',
      main: '#03713D',
      dark: '#0c421a',
      contrastText: '#fff'
    },
    secondary: {
      light: '#339ba5',
      main: '#03713D',
      dark: '#071884',
      contrastText: '#000'
    },
    success: {
      light: '#3BE481',
      main: '#4EAE5B'
    },
    error: {
      main: '#EA3323'
    }
  }
})
