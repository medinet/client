const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin')

const { src, root } = require('./paths');

module.exports = options => ({
  context: src,
  entry: ['babel-polyfill', './index.jsx'],
  mode: options.env,
  resolve: {
    modules: [src, 'node_modules'],
    extensions: ['.js', '.jsx', '.json', '.css'],
    alias: { "@root": src }
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        use: 'babel-loader',
        exclude: /(node_modules)/,
        include: src
      },
      {
        test: /\.css$/,
        exclude: /\.module\.css$/,
        use: ['style-loader','css-loader']
      },
      {
        test: /\.(png|gif|jpe?g|svg|ttf|eot|otf|woff2)$/,
        use: 'url-loader?limit=10000&name=[path][name].[hash:base64:5].[ext]'
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.REACT_APP_API_URL': JSON.stringify(process.env.REACT_APP_API_URL)
    }),
    new HtmlWebpackPlugin({
      title: 'Medinet',
      template: 'index.html',
      filename: 'index.html',
      favicon: root('favicon.ico'),
      meta: {
        viewport: 'width=device-width, initial-scale=1, shrink-to-fit=no',
        "theme-color": '#ffffff'
      },
      minify: (options.env === 'production') && {
        caseSensitive: true,
        collapseWhitespace: true,
        keepClosingSlash: true
      }
    }),
    new CopyWebpackPlugin([
      { from: root('src/assets'), to: 'assets' }
    ])
  ]
});
