const webpackMerge = require('webpack-merge');

const { dist } = require('./paths');
const commonConfig = require('./webpack.common.js');

module.exports = webpackMerge(
  commonConfig({ env: 'development' }),
  {
    // devtool: 'inline-source-map',
    output: {
      path: dist,
      publicPath: '/',
      filename: 'static/bundle.[name].js',
      chunkFilename: 'static/chunk.[name].js',
      library: '[name]',
      hashDigest: 'hex',
      hashDigestLength: 8
    },
    devServer: {
      port: process.env.PORT,
      host: '0.0.0.0',
      public: `localhost:${process.env.EXTERNAL_PORT}`,
      inline: true,
      progress: true,
      stats: {
        chunks: false,
        children: false
      },
      historyApiFallback: true
    }
  }
);
