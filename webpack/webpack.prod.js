const webpackMerge = require('webpack-merge');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const CompressionPlugin = require('compression-webpack-plugin');

const { dist } = require('./paths');
const commonConfig = require('./webpack.common.js');


module.exports = webpackMerge(
  commonConfig({ env: 'production' }),
  {
    output: {
      path: dist,
      publicPath: '/',
      filename: 'static/bundle.[name].js',
      chunkFilename: 'static/chunk.[name].js',
      library: '[name]',
      hashDigest: 'hex',
      hashDigestLength: 8
    },
    performance: {
      maxEntrypointSize: 1000000,
      maxAssetSize: 300000,
      assetFilter(assetFilename) {
        return (/\.(?:js|css)\.gz$/.test(assetFilename));
      }
    },
    optimization: {
      splitChunks: {
        cacheGroups: {
          vendors: {
            test: /[\\/]node_modules[\\/]/,
            name: 'vendors',
            chunks: 'all'
          }
        }
      }
    },
    plugins: [
      new CompressionPlugin({
        algorithm: 'gzip',
        test: /\.(js|css|svg)/
      }),
      new BundleAnalyzerPlugin({
        analyzerMode: 'disable'
      })
    ]
  }
);

